import observer.Dwarf;
import observer.Santa;
import observer.Observer;

public class SantaWorkshopMain {

	public static void main(String[] args) {
		
		Santa santa = new Santa();
		
		Observer dwarf = new Dwarf();
		
		santa.observable.subscribe(dwarf);
		
		santa.sendNeedDollCommand();
		santa.sendNeedWheelCommand();
		
		santa.sendNeedDollCommand();
	}

}
