package abstractfactory;

public class FactoryProducer {
	
	private SantaFactory factory;
	private SantaToy toy;
	
	public FactoryProducer(SantaFactory factory) {
		this.factory = factory;
	}
	
	public void createToy() {
		this.toy = factory.createToy();
	}
	
	public void build() {
		this.toy.build();
	}
}
