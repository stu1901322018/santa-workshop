package abstractfactory;

public class SantaDollFactory implements SantaFactory {

	@Override
	public SantaToy createToy() {
		return new SantaDoll();
	}

}
