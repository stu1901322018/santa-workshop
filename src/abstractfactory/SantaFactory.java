package abstractfactory;

public interface SantaFactory {
	
	public SantaToy createToy();

}
