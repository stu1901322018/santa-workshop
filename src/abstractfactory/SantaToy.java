package abstractfactory;

public interface SantaToy {
	
	public void build();
	
}
