package abstractfactory;

public class SantaWheelFactory implements SantaFactory {

	@Override
	public SantaToy createToy() {
		return new SantaWheel();
	}

}
