package command;

import abstractfactory.FactoryProducer;
import abstractfactory.SantaFactory;

public class CreateToyCommand implements Command {
	
	private SantaFactory santaFactory;
	
	public CreateToyCommand(SantaFactory santaFactory) {
		this.santaFactory = santaFactory;
	}

	@Override
	public void execute() {
		FactoryProducer factoryProducer = new FactoryProducer(this.santaFactory);
		
		factoryProducer.createToy();
		factoryProducer.build();
	}

}
