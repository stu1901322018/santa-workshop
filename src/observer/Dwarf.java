package observer;

import command.Command;

public class Dwarf implements Observer {

	@Override
	public void update(Command command) {
		command.execute();
	}

}
