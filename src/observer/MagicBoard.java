package observer;

import java.util.ArrayList;
import java.util.List;

import abstractfactory.FactoryProducer;
import abstractfactory.SantaDollFactory;
import abstractfactory.SantaFactory;
import abstractfactory.SantaWheelFactory;
import command.CreateToyCommand;

public class MagicBoard implements Observable {
	
	private List<Observer> observers;
	
	public MagicBoard() {
		this.observers = new ArrayList<>();
	}

	@Override
	public void subscribe(Observer observer) {
		this.observers.add(observer);
	}

	@Override
	public void unsubscribe(Observer observer) {
		this.observers.remove(observer);
	}

	@Override
	public void notifyObservers(SantaFactory factory) {
		if (factory == null) {
			return;
		}
		
		CreateToyCommand command = new CreateToyCommand(factory);
		
		for(Observer observer : this.observers) {
			observer.update(command);
		}
	}

}
