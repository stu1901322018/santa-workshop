package observer;

import abstractfactory.SantaFactory;

public interface Observable {
	
	public void subscribe(Observer observer);
	public void unsubscribe(Observer observer);
	public void notifyObservers(SantaFactory factory);
}
