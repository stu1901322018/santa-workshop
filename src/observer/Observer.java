package observer;

import command.Command;

public interface Observer {
	
	void update(Command command);
	
}
