package observer;

import abstractfactory.SantaDollFactory;
import abstractfactory.SantaFactory;
import abstractfactory.SantaWheelFactory;

public class Santa {

	public Observable observable;

	public Santa() {
		this.observable = new MagicBoard();
	}

	public void sendNeedDollCommand() {
		SantaFactory factory = this.getSantaFactory("Трябват ми кукли");

		observable.notifyObservers(factory);
	}

	public void sendNeedWheelCommand() {
		SantaFactory factory = this.getSantaFactory("Трябват ми колелета");

		observable.notifyObservers(factory);
	}

	private SantaFactory getSantaFactory(String command) {
		SantaFactory factory = null;

		if (command.equals("Трябват ми кукли")) {
			factory = new SantaDollFactory();
		} else if (command.equals("Трябват ми колелета")) {
			factory = new SantaWheelFactory();
		} else {
			System.out.println("Непозната играчка!");
		}

		return factory;
	}
}
